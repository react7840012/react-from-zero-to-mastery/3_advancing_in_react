import React from 'react'

const UserDetails = ({person}) => {

    function driverAvailable(age) {

        return age >= 18 ? "Yes" : "No"

    }

  return (
    <div>
        <p><h3>Name: {person.name}</h3></p>
        <p>Age: {person.age}</p>
        <p>Profession: {person.profession}</p>
        <p><h4>Can obtain driver's license: {driverAvailable(person.age)}</h4></p>        
    </div>
  )
}

export default UserDetails