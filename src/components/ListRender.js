import {useState} from 'react'

export const ListRender = () => {
  
    const [list] = useState(["Fernando", "Vinicius", "Leticia", "Josias"]);

    const [users, setUsers] = useState([
        {id: 1, name: "Fernando", age: 32},
        {id: 2, name: "Leticia", age: 23},
        {id: 3, name: "Vinicius", age: 18}
    ])

    const deleteRandom = () => {
        const randomNumber = Math.floor(Math.random() * 4);

        setUsers((prevUsers) => {
            return prevUsers.filter((user) => randomNumber !== user.id)
        })
    }

    return <div>
        <ul>
            {list.map((item, index) => (
                
                <li key={index}>{item}</li>
            ))}
        </ul>
        <ul>
            {users.map(user => (
                <li key={user.id}>{user.name}</li>
            ))}
        </ul>
        <button onClick={deleteRandom}>Delete random user</button>
    </div>
}

export default ListRender;
