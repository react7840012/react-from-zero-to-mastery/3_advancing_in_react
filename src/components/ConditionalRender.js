import { useState } from "react"

const ConditionalRender = () => {

    const [x] = useState(false);

    const [name, setName] = useState("John")

  return (
    <div>
        <h1>Will this be exhibited?</h1>
        { x && <p>If x is true, then yes!</p>}
        { !x && <p>Not now, cze X is false</p>}
        {name === 'David' ? (
            <div>
                <p>The name is David</p>
            </div>
        ) : (
            <div>
                <p>Name not found!</p>
            </div>
        )}
        <button onClick={() => setName('David')}>Click here!</button>
    </div>
  )
}

export default ConditionalRender