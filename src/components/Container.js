import React from 'react'

function Container({children, myValue}) {
  return (
    <div>
        <h2>This is the container's title</h2>
        {children}
        <p>The value is: {myValue}</p>
    </div>
  )
}

export default Container