

const CarDetails = ({color, km, brand, newCar}) => {
  return (
    <div>
        <h2>Car Details</h2>
        <ul>
            <li>Brand: {brand}</li>
            <li>KM: {km}</li>
            <li>Color: {color}</li>
        </ul>
        {newCar && <p>This is a new Car!</p>}
    </div>
  )
}

export default CarDetails