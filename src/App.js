import { useState } from 'react';
import './App.css';

import City from "./assets/city.jpg";
import CarDetails from './components/CarDetails';
import ChangeMessageState from './components/ChangeMessageState';
import ConditionalRender from './components/ConditionalRender';
import Container from './components/Container';
import ExecuteFunction from './components/ExecuteFunction';
import Fragment from './components/Fragment';
import ListRender from './components/ListRender';
import ManageData from './components/ManageData';
import Message from './components/Message';
import ShowUserName from './components/ShowUserName';
import UserDetails from './components/UserDetails';


function App() {

  const [userName] = useState("Maria");

  const cars = [
    {id: 1, brand: "Hyundai", color: "Yellow", newCar: true, km: 0},
    {id: 2, brand: "Kia", color: "Black", newCar: false, km: 73293},
    {id: 3, brand: "Renault", color: "Blue", newCar: false, km: 30282},
  ]

  function showMessage() {
    console.log("Event in the father component ")
  }

  const [message, setMessage] = useState("")

  const handleMessage = (msg) => {
    setMessage(msg);
  }

  const people = [
    {id: 1, name: 'Fernando', age: 32, profession: "Software Developer"},
    {id: 2, name: 'Peter', age: 15, profession: "Student"},
    {id: 3, name: 'John', age: 17, profession: "Student"},
    {id: 4, name: 'Maria', age: 23, profession: "Dentist"},
  ];

  return (
    <div className="App">
      <h1>Avançando em React</h1>
      {/* Image at public*/}
      <div>
        <img src="/img1.jpg" alt="Paisagem" />
      </div>
      {/* {Image at Asset} */}       
      <div>
        <img src={City} alt="Cidade" />
      </div>
      <ManageData/>
      <ListRender/>
      <ConditionalRender/>
      {/* props */}
      <ShowUserName name={userName} />
      {/* destructuring */}
      <CarDetails brand="VW" km={123000} color="Dark Blue" newCar={false} />
      {/* reuse of components */}
      <CarDetails brand="Ford" km={0} color="Green" newCar={true} />
      <CarDetails brand="Fiat" km={38000} color="White" newCar={false} />
      {/* loop with array of objects */}
      {cars.map((car) => (
        <CarDetails key={car.id} brand={car.brand} color={car.color} km={car.km} newCar={car.newCar} />
      ))}
      {/* fragments */}
      <Fragment propFragment="test"/>
      {/* children */}
      <Container myValue={123}>
        <p>This is the content</p>
      </Container>
      {/* function prop */}
      <ExecuteFunction myFunction={showMessage}/>
      {/* state lift */}
      <Message msg={message}/>
      <ChangeMessageState handleMessage={handleMessage}/>

      {people.map( (person) => (
        <UserDetails person={person} />
      ))}
      
    </div>
  );
}

export default App;
